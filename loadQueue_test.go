package qaw

/*
import (
	"testing"
	"github.com/nu7hatch/gouuid"
)

type testFetcher struct { }

type testData struct {
	id *uuid.UUID
}

func (t *testFetcher) GetObject(id *uuid.UUID) interface{} {
	return testData{id}
}

func TestCreate(t *testing.T) {
	fetcher, err := NewFileStorerFetcher("loadQueue", "master.txt")
	if err != nil { t.Errorf("Error: %q", err.Error()) }
	actioner := NewQAW(fetcher)
	if actioner.Size() != 0 {
		t.Errorf("Error: Size=%d", actioner.Size())
	}
	if actioner.Limit() > 0 {
		t.Errorf("Error: Limit=%d", actioner.Limit())
	}
}

func TestCreateLimit(t *testing.T) {
	fetcher, err := NewFileStorerFetcher("loadQueue", "master.txt")
	if err != nil { t.Errorf("Error: %q", err.Error()) }
	actioner := NewLimitedQAW(50, fetcher)
	if actioner.Size() != 0 {
		t.Errorf("Error: Size=%d", actioner.Size())
	}
	if actioner.Limit() != 50 {
		t.Errorf("Error: Limit=%d", actioner.Limit())
	}
}

func TestAdd(t *testing.T) {
	testID, err := uuid.NewV4()
	if err != nil {
		t.Errorf("UUID creation failure")
	}
	fetcher, err := NewFileStorerFetcher("loadQueue", "master.txt")
	if err != nil { t.Errorf("Error: %q", err.Error()) }
	actioner := NewQAW(fetcher)
	if actioner.Size() != 0 {
		t.Errorf("Error: Size=%d", actioner.Size())
	}
	if !actioner.QueueLoad(testID) {
		t.Errorf("Failed to queue!")
	}
	if actioner.Size() != 1 {
		t.Errorf("Error: Size=%d", actioner.Size())
	}
}

func TestAddLimit(t *testing.T) {
	testID, err := uuid.NewV4()
	if err != nil {
		t.Errorf("UUID creation failure")
	}
	fetcher, err := NewFileStorerFetcher("loadQueue", "master.txt")
	if err != nil { t.Errorf("Error: %q", err.Error()) }
	actioner := NewQAW(fetcher)
	if actioner.Size() != 0 {
		t.Errorf("Error: Size=%d", actioner.Size())
	}
	if !actioner.QueueLoad(testID) {
		t.Errorf("Failed to queue!")
	}
	if actioner.Size() != 1 {
		t.Errorf("Error: Size=%d", actioner.Size())
	}
}

func TestNoExecute(t *testing.T) {
	fetcher, err := NewFileStorerFetcher("loadQueue", "master.txt")
	if err != nil { t.Errorf("Error: %q", err.Error()) }
	actioner := NewQAW(fetcher)
	if actioner.Size() != 0 {
		t.Errorf("Error: Size=%d", actioner.Size())
	}
	outMap := actioner.Action()
	if len(outMap) != 0 {
		t.Errorf("Error: Map Size=%d", len(outMap))
	}
}

func TestExecuteSingle(t *testing.T) {
	testID, err := uuid.NewV4()
	if err != nil {
		t.Errorf("UUID creation failure")
	}
	fetcher, err := NewFileStorerFetcher("loadQueue", "master.txt")
	if err != nil { t.Errorf("Error: %q", err.Error()) }
	actioner := NewQAW(fetcher)
	if actioner.Size() != 0 {
		t.Errorf("Error: Size=%d", actioner.Size())
	}
	if !actioner.QueueLoad(testID) {
		t.Errorf("Failed to queue!")
	}
	if actioner.Size() != 1 {
		t.Errorf("Error: Size=%d", actioner.Size())
	}
	outMap := actioner.Action()
	if len(outMap) != 1 {
		t.Errorf("Error: Map Size=%d", len(outMap))
	}
	for key, val := range outMap {
		t.Logf("(id=%s, mapVal=%v)", key.String(), val)
	}
	if actioner.Size() != 0 {
		t.Errorf("Error: Size=%d", actioner.Size())
	}
}

func TestExecuteMultiple(t *testing.T) {
	num := 100
	inputArr := make([]*uuid.UUID, num)
	for i:=0; i < num; i++ {
		testID, err := uuid.NewV4()
		if err != nil {
			t.Errorf("UUID creation failure")
		} else {
			inputArr[i] = testID
		}
	}
	fetcher, err := NewFileStorerFetcher("loadQueue", "master.txt")
	if err != nil { t.Errorf("Error: %q", err.Error()) }
	actioner := NewQAW(fetcher)
	if actioner.Size() != 0 {
		t.Errorf("Error: Size=%d", actioner.Size())
	}
	for i:=0; i < num; i++ {
		if !actioner.QueueLoad(inputArr[i]) {
			t.Errorf("Failed to queue!")
		}
		if actioner.Size() != i+1 {
			t.Errorf("Error: i+1=%d, Size=%d", i+1, actioner.Size())
		}
	}
	outMap := actioner.Action()
	if len(outMap) != num {
		t.Errorf("Error: Map Size=%d", len(outMap))
	}
	for key, val := range outMap {
		t.Logf("(id=%s, mapVal=%v)", key.String(), val)
	}
	if actioner.Size() != 0 {
		t.Errorf("Error: Size=%d", actioner.Size())
	}
}

func BenchmarkHello(b *testing.B) {
    inputArr := make([]*uuid.UUID, b.N)
	for i:=0; i < b.N; i++ {
		testID, err := uuid.NewV4()
		if err != nil {
			b.Errorf("UUID creation failure")
		} else {
			inputArr[i] = testID
		}
	}
	fetcher, err := NewFileStorerFetcher("loadQueue", "master.txt")
	if err != nil { b.Errorf("Error: %q", err.Error()) }
	actioner := NewQAW(fetcher)
	for i:=0; i < b.N; i++ {
		if !actioner.QueueLoad(inputArr[i]) {
			b.Errorf("Failed to queue!")
		}
	}
	b.ResetTimer()
	_ = actioner.Action()
}*/
