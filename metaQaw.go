package qaw

import (
	"errors"
	"github.com/nu7hatch/gouuid"
	"reflect"
	"strings"
)

type Identifiable interface {
	ID() *uuid.UUID
}

type MetaStorager struct {
	database      Storager
	typeToFunc    map[string]func() interface{}
	fileExtension string
	metadataPost  string
}

type metaStorageData struct {
	ObjName string
}

func NewMetaStorager(db Storager, fileExtension, metadataPost string) *MetaStorager {
	temp := MetaStorager{db, make(map[string]func() interface{}), fileExtension, metadataPost}
	return &temp
}

func (m *MetaStorager) getMetaType(obj interface{}) string {
	return reflect.ValueOf(obj).Type().String()
}

func (m *MetaStorager) HandleStorageType(funcCreator func() interface{}) {
	m.typeToFunc[m.getMetaType(funcCreator())] = funcCreator
}

func (m *MetaStorager) SaveObject(path string, object Identifiable) error {
	err := m.database.Write(path, object.ID().String(), m.fileExtension, object)
	if err != nil {
		return err
	}
	err = m.database.Write(path, object.ID().String()+m.metadataPost, m.fileExtension, metaStorageData{m.getMetaType(object)})
	return err
}

func (m *MetaStorager) LoadObject(path string, id *uuid.UUID) (object interface{}, e error) {
	if id == nil {
		return nil, errors.New("LoadObject error: nil passed for id")
	}
	temp := metaStorageData{}
	err := m.database.ReadObj(path, id.String()+m.metadataPost, m.fileExtension, &temp)
	if err != nil {
		return nil, err
	}
	fun, ok := m.typeToFunc[temp.ObjName]
	if !ok {
		return nil, errors.New("LoadObject error: Cannot find a constructor in the meta data map for: " + temp.ObjName)
	}
	newObj := fun()
	err = m.database.ReadObj(path, id.String(), m.fileExtension, newObj)
	return newObj, err
}

func (m *MetaStorager) ListAllIds(path string) ([]string, error) {
	filenames, err := m.database.ListFilenames(path)
	if err != nil {
		return nil, err
	}
	var nonMetaFiles []string = nil
	for _, file := range filenames {
		if !strings.Contains(file, m.metadataPost) {
			nonMetaFiles = append(nonMetaFiles, file)
		}
	}
	return nonMetaFiles, nil
}

func (m *MetaStorager) Start() {
	m.database.Start()
}

func (m *MetaStorager) Stop() {
	m.database.Stop()
}
