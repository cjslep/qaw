// Package qaw provides an engine for basic database read/write operations.
package qaw

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	"time"
)

const (
	FOLDER_PERMISSIONS = 0755
	FILE_PERMISSIONS   = 0666
	MASTER             = "master"
	DATA               = "data"
)

// Storager is the top level interface for interacting with the data store, whatever
// its underlying implementation may be. A Storager is able to Start and Stop manually and
// repeatedly in order to service requests.
type Storager interface {

	// Write places an object to the specified filename and extension. The path acts allows
	// the client to separate different stored object types to different folder locations.
	Write(path, filename, extension string, obj interface{}) error

	// Read reads the specified file into a map for general retrieval, returning an error
	// if unable.
	Read(path, filename, extension string) (obj map[string]interface{}, err error)

	// ReadObj fetches the specified file and puts the results into the specified object, returning
	// an error if reading fails.
	ReadObj(path, filename, extension string, inter interface{}) error

	// ListFilenames attains a list of names of objects stored at the specified path,
	// allowing retrieval of a group of objects.
	ListFilenames(path string) ([]string, error)

	// Starts up the storager so it can start servicing requests.
	Start()

	// Shuts down the storager so it stops servicing requests.
	Stop()
}

// Marshaller functions take an object and translate it into
// an array of bytes.
type Marshaller func(v interface{}) (b []byte, e error)

// Unmarshaller functions take an array of bytes and translate
// the values into the specified object.
type Unmarshaller func(data []byte, v interface{}) error

// FlatFileStorer allows for any sort of Marshaller or Unmarshaller to be used
// when storing and loading flat files. It creates and updates data only; it never
// deletes files. Therefore, updates have a newer timestamp appended to their filename
// so data can be profiled over time in whole. This uses a lot of space and means that
// a read while a write is occurring will pull old data. It uses a stateful
// goroutine in order to synchronize, which slows it to take about 10ms per Write() call
// when hit with 200 goroutines writing to a single file (3/24/2104).
type FlatFileStorer struct {
	allPrePath      string
	masterFile      string
	dataChanger     Marshaller
	objectChanger   Unmarshaller
	latestData      map[string]time.Time
	writeToFile     chan *writeOp
	doneWriteToFile chan string
	shutdown        chan bool
	updateMaster    chan *updateMasterOp
	latestFilename  chan *latestFilename
	isShutdown      bool
}

// writeOp specifies what the stateful goroutine needs in order to orchestrate which other
// goroutines are allowed to write without causing data racing conflicts
type writeOp struct {
	OKBeginWrite chan<- bool
	File         string
}

// updateMasterOp specifies what the stateful goroutine needs in order to update the master
// file without having data racing conflicts
type updateMasterOp struct {
	TimeUpdate  time.Time
	File        string
	ErrResponse chan<- error
}

// latestFilename specifies what the stateful goroutine needs in order to fetch the
// latest file without having data racing conflicts.
type latestFilename struct {
	File     string
	Response chan<- *latestFilenameResponse
}

// latestFilenameResponse specifies what is returned by the stateful goroutine when
// fetching the latest file
type latestFilenameResponse struct {
	LatestFilename string
	Err            error
}

// NewJSONFileStorer creates a new Storager with the database root at the specified head
// and given master data file, which cannot be empty. A nil error is returned if the Storager
// was properly initialized.
func NewJSONFileStorer(head, masterFile string) (s *FlatFileStorer, e error) {
	if masterFile == "" {
		return nil, errors.New("Cannot create a new Storager with an empty master file name.")
	}
	temp := FlatFileStorer{head, masterFile, json.Marshal, json.Unmarshal, nil, make(chan *writeOp, 0), make(chan string, 0), make(chan bool, 0), make(chan *updateMasterOp, 0), make(chan *latestFilename, 0), true}
	e = temp.loadPreexistingMasterData()
	if e != nil {
		return nil, e
	}
	return &temp, e
}

// NewJSONFileStorerFromConfig creates a new Storager from the specified config file
func NewJSONFileStorerFromConfig(filename string) (s *FlatFileStorer, e error) {
	config, err := LoadDataConfig(filename)
	if err != nil {
		return nil, err
	}
	return NewJSONFileStorer(config.DatabasePath, config.MasterFile)
}

// Read parses the data in the file with the specified extension in the path grouping to a map
// for generic usage. A successful read will not have an error returned. This function is
// concurrent-safe, but not guaranteed to retrieve the latest result if a write is currently
// in progress for the same file.
func (f *FlatFileStorer) Read(path, filename, extension string) (obj map[string]interface{}, err error) {
	if f.isShutdown {
		return nil, errors.New("FlatFileStorer is shutdown, will not handle new requests.")
	}
	modPath, err := f.getFolderAndFile(path, filename)
	if err != nil {
		return nil, err
	}
	modFileName, err := f.getLatestFile(filename)
	if err != nil {
		return nil, err
	}
	sepString, err := strconv.Unquote(strconv.QuoteRune(os.PathSeparator))
	if err != nil {
		return nil, err
	}
	b, err := ioutil.ReadFile(modPath + sepString + modFileName + extension)
	if err != nil {
		return nil, err
	}
	var outObj interface{}
	err = f.objectChanger(b, &outObj)
	return outObj.(map[string]interface{}), err
}

// ReadObj parses the data in the file with the specified extension in the path grouping into
// an interface instance, allowing for dynamic-type loading. A successful read will not have
// an error returned. This function is concurrent-safe, but not guaranteed to retrieve
// the latest result if a write is currently in progress for the same file.
func (f *FlatFileStorer) ReadObj(path, filename, extension string, inter interface{}) error {
	if f.isShutdown {
		return errors.New("FlatFileStorer is shutdown, will not handle new requests.")
	}
	modPath, err := f.getFolderAndFile(path, filename)
	if err != nil {
		return err
	}
	modFileName, err := f.getLatestFile(filename)
	if err != nil {
		return err
	}
	sepString, err := strconv.Unquote(strconv.QuoteRune(os.PathSeparator))
	if err != nil {
		return err
	}
	b, err := ioutil.ReadFile(modPath + sepString + modFileName + extension)
	if err != nil {
		return err
	}
	err = f.objectChanger(b, inter)
	return err
}

// Start beings the stateful goroutine that provides concurrent-safe access to the filesystem,
// reading, and writing. It quits partially gracefully without interrupting other writing goroutines.
//
// TODO: Quit gracefully for reading goroutines as well.
func (f *FlatFileStorer) Start() {
	go func() {
		var queuedWrites = make(map[string][]chan<- bool)
		var currentWrites = make(map[string]bool)
		var quit = false
		for {
			select {
			case writeOp := <-f.writeToFile:
				if respChanArr, ok := queuedWrites[writeOp.File]; ok {
					respChanArr = append(respChanArr, writeOp.OKBeginWrite)
					queuedWrites[writeOp.File] = respChanArr
				} else {
					if _, ok := currentWrites[writeOp.File]; ok {
						queuedWrites[writeOp.File] = append(queuedWrites[writeOp.File], writeOp.OKBeginWrite)
					} else {
						currentWrites[writeOp.File] = true
						writeOp.OKBeginWrite <- true
					}
				}
			case doneFile := <-f.doneWriteToFile:
				delete(currentWrites, doneFile)
				if respChanArr, ok := queuedWrites[doneFile]; ok {
					okGoWrite, respChanArr := respChanArr[len(respChanArr)-1], respChanArr[:len(respChanArr)-1]
					currentWrites[doneFile] = true
					okGoWrite <- true
					if len(respChanArr) > 0 {
						queuedWrites[doneFile] = respChanArr
					} else {
						delete(queuedWrites, doneFile)
					}
				}
				if quit && len(queuedWrites) == 0 && len(currentWrites) == 0 {
					return
				}
			case update := <-f.updateMaster:
				f.latestData[update.File] = update.TimeUpdate
				update.ErrResponse <- f.saveLatestMasterData()
			case getLatest := <-f.latestFilename:
				if _, ok := f.latestData[getLatest.File]; !ok {
					getLatest.Response <- &latestFilenameResponse{"", errors.New("No such file: " + getLatest.File)}
				} else {
					getLatest.Response <- &latestFilenameResponse{f.filenameFromTime(getLatest.File, f.latestData[getLatest.File]), nil}
				}
			case quit = <-f.shutdown:
				if quit && len(queuedWrites) == 0 && len(currentWrites) == 0 {
					return
				}
			}
		}
	}()
	f.isShutdown = false
}

// Stop prevents future calls to the filesystem and notifies the stateful
// goroutine to shut down gracefully.
func (f *FlatFileStorer) Stop() {
	f.isShutdown = true
	f.shutdown <- true
}

// ListFilenames gets a list of all stored objects within the specified path, assuming each
// subdirectory contains an object. Clients can then iteratively search and load the group as
// necessary. A successful retrieval of object names returns error as nil.
func (f *FlatFileStorer) ListFilenames(path string) ([]string, error) {
	if f.isShutdown {
		return nil, errors.New("FlatFileStorer is shutdown, will not handle new requests.")
	}
	fullPath, e := f.getFolder(path)
	if e != nil {
		return nil, e
	}
	dirsAndFiles, e := ioutil.ReadDir(fullPath)
	if e != nil {
		return nil, e
	}
	outString := make([]string, len(dirsAndFiles))
	indexOutString := 0
	for _, fileInfo := range dirsAndFiles {
		if fileInfo.IsDir() {
			outString[indexOutString] = fileInfo.Name()
			indexOutString++
		}
	}
	if indexOutString > 0 {
		outString = outString[:indexOutString]
	}
	return outString, nil
}

// Write takes an object and serializes it to the filename within a path group. It is
// concurrent-safe. A successful write will not have an error returned.
func (f *FlatFileStorer) Write(path, filename, extension string, obj interface{}) error {
	if f.isShutdown {
		return errors.New("FlatFileStorer is shutdown, will not handle new requests.")
	}
	okToWrite := make(chan bool, 0)
	f.writeToFile <- &writeOp{okToWrite, filename}
	<-okToWrite
	err := f.write(path, filename, extension, obj)
	f.doneWriteToFile <- filename
	return err
}

// Helper that needs to be protected by the stateful goroutine in Start so multiple writes do not occur
// for the same file simultaneously.
func (f *FlatFileStorer) write(path, filename, extension string, obj interface{}) error {
	modPath, err := f.getFolderAndFile(path, filename)
	if err != nil {
		return err
	}
	err = os.MkdirAll(modPath, FOLDER_PERMISSIONS)
	if err != nil {
		return err
	}
	modFileName, newTime := f.newFilename(filename)
	sepString, err := strconv.Unquote(strconv.QuoteRune(os.PathSeparator))
	if err != nil {
		return err
	}
	file, err := os.OpenFile(modPath+sepString+modFileName+extension, os.O_CREATE|os.O_WRONLY, FILE_PERMISSIONS)
	if err != nil {
		return err
	}
	defer file.Close()
	byteOut, err := f.dataChanger(obj)
	if err != nil {
		return err
	}
	_, err = file.Write(byteOut)
	f.updateFile(filename, newTime)
	return err
}

// getLatestFile is a helper that utilizes the stateful goroutine to fetch the latest filename
func (f *FlatFileStorer) getLatestFile(filename string) (s string, e error) {
	response := make(chan *latestFilenameResponse)
	f.latestFilename <- &latestFilename{filename, response}
	respData := <-response
	return respData.LatestFilename, respData.Err
}

// updateFile is a helper that triggers the master file update in the stateful goroutine
func (f *FlatFileStorer) updateFile(filename string, t time.Time) error {
	errResponse := make(chan error)
	f.updateMaster <- &updateMasterOp{t, filename, errResponse}
	return <-errResponse
}

// newFilename is a helper that gets the latest filename based on the current time
func (f *FlatFileStorer) newFilename(filename string) (s string, t time.Time) {
	t = time.Now()
	s = f.filenameFromTime(filename, t)
	return
}

// getFolderAndFile is a utility helper that gets the full path for a particular file
func (f *FlatFileStorer) getFolderAndFile(path, filename string) (s string, e error) {
	strSlice := make([]string, 4)
	strSlice[0] = f.allPrePath
	strSlice[1] = DATA
	strSlice[2] = path
	strSlice[3] = filename
	sepString, err := strconv.Unquote(strconv.QuoteRune(os.PathSeparator))
	if err != nil {
		return "", err
	}
	return strings.Join(strSlice, sepString), nil
}

// getFolderAndFile is a utility helper that gets the full path to a directory
func (f *FlatFileStorer) getFolder(path string) (s string, e error) {
	strSlice := make([]string, 3)
	strSlice[0] = f.allPrePath
	strSlice[1] = DATA
	strSlice[2] = path
	sepString, err := strconv.Unquote(strconv.QuoteRune(os.PathSeparator))
	if err != nil {
		return "", err
	}
	return strings.Join(strSlice, sepString), nil
}

// getFolderAndFile is a utility helper that gets the full path to the master directory
func (f *FlatFileStorer) getMasterFolder() (s string, e error) {
	strSlice := make([]string, 2)
	strSlice[0] = f.allPrePath
	strSlice[1] = MASTER
	sepString, err := strconv.Unquote(strconv.QuoteRune(os.PathSeparator))
	if err != nil {
		return "", err
	}
	return strings.Join(strSlice, sepString), nil
}

func (f *FlatFileStorer) filenameFromTime(filename string, lastUpdate time.Time) string {
	return strings.Replace(strings.Replace(f.timeFormat(lastUpdate)+"_"+filename, " ", "_", -1), ":", "-", -1)
}

func (f *FlatFileStorer) timeFormat(t time.Time) string {
	return strings.Replace(strings.Replace(t.Local().Format(time.StampNano), " ", "_", -1), ":", "-", -1)
}

func (f *FlatFileStorer) saveLatestMasterData() error {
	oldMasterTime := time.Now()
	newFile := f.filenameFromTime(f.masterFile, oldMasterTime)
	sepString, err := strconv.Unquote(strconv.QuoteRune(os.PathSeparator))
	if err != nil {
		return err
	}
	masterPath, err := f.getMasterFolder()
	if err != nil {
		return err
	}
	masterPath += sepString
	file, err := os.OpenFile(masterPath+"new"+newFile, os.O_CREATE|os.O_WRONLY, FILE_PERMISSIONS)
	if err != nil {
		return err
	}
	byteOut, err := f.dataChanger(f.latestData)
	if err != nil {
		return err
	}
	_, err = file.Write(byteOut)
	if err != nil {
		return err
	}
	file.Close()
	err = os.Rename(masterPath+f.masterFile, masterPath+newFile)
	if err != nil {
		return err
	}
	err = os.Rename(masterPath+"new"+newFile, masterPath+f.masterFile)
	return err
}

func (f *FlatFileStorer) loadPreexistingMasterData() error {
	if f.latestData != nil {
		return errors.New("Preexisting data already loaded!")
	} else {
		f.latestData = make(map[string]time.Time)
	}
	sepString, err := strconv.Unquote(strconv.QuoteRune(os.PathSeparator))
	if err != nil {
		return err
	}
	masterPath, err := f.getMasterFolder()
	if err != nil {
		return err
	}
	err = os.MkdirAll(masterPath, FOLDER_PERMISSIONS)
	if err != nil {
		return err
	}
	masterPath += sepString
	temp, err := os.OpenFile(masterPath+f.masterFile, os.O_RDONLY, FILE_PERMISSIONS)
	if err != nil && os.IsNotExist(err) {
		file, err := os.OpenFile(masterPath+f.masterFile, os.O_CREATE|os.O_EXCL|os.O_WRONLY, FILE_PERMISSIONS)
		if err != nil && !os.IsExist(err) {
			return err
		}
		file.Close()
		return nil
	} else if err != nil {
		return err
	}
	temp.Close()
	b, err := ioutil.ReadFile(masterPath + f.masterFile)
	if err != nil {
		return err
	}
	if len(b) == 0 {
		return nil
	}
	var outObj interface{}
	err = f.objectChanger(b, &outObj)
	if err != nil {
		return err
	}
	tempMap, ok := outObj.(map[string]interface{})
	if !ok {
		return errors.New("Error converting master file to map[string]interface{}!")
	}
	errSlice := make([]string, 0)
	for key, val := range tempMap {
		switch vval := val.(type) {
		case string:
			t, err := time.Parse(time.RFC3339Nano, vval)
			if err != nil {
				errSlice = append(errSlice, key)
			}
			f.latestData[key] = t
		default:
			errSlice = append(errSlice, key)
		}
	}
	if len(errSlice) > 0 {
		return errors.New("Could not convert the following files to times: " + strings.Join(errSlice, ", "))
	}
	return nil
}
