package qaw

import (
	"bitbucket.org/cjslep/dailyLogger"
)

type LoadQueuer interface {
	Size() int
	QueueLoad(id string) bool
	Action() map[string]interface{}
	ActionObject(objFactory func() interface{}) map[string]interface{}
}

type qawLoader struct {
	path       string
	extension  string
	toLoad     map[string]interface{}
	myStorager Storager
	logger     *dailyLogger.DailyLogger
}

type kvPair struct {
	ID    string
	Value interface{}
}

func (q *qawLoader) Size() int {
	return len(q.toLoad)
}

func (q *qawLoader) QueueLoad(id string) bool {
	_, ok := q.toLoad[id]
	if !ok {
		q.toLoad[id] = nil
		return true
	}
	return false
}

func (q *qawLoader) Action() map[string]interface{} {
	testChan := make(chan kvPair, len(q.toLoad))
	for key, _ := range q.toLoad {
		go func(path, file, ext string, l *dailyLogger.DailyLogger) {
			obj, err := q.myStorager.Read(path, file, ext)
			if err != nil {
				if l != nil {
					l.Println(err.Error())
				}
				testChan <- kvPair{"", nil}
				return
			}
			pair := kvPair{file, obj}
			testChan <- pair
		}(q.path, key, q.extension, q.logger)
	}
	waitAmount := len(q.toLoad)
	for waitAmount > 0 {
		temp := <-testChan
		if temp.Value != nil && temp.ID != "" {
			q.toLoad[temp.ID] = temp.Value
		}
		waitAmount--
	}
	outMap := q.toLoad
	q.toLoad = make(map[string]interface{})
	return outMap
}

func (q *qawLoader) ActionObject(objFactory func() interface{}) map[string]interface{} {
	testChan := make(chan kvPair, len(q.toLoad))
	for key, _ := range q.toLoad {
		go func(path, file, ext string, obj interface{}, l *dailyLogger.DailyLogger) {
			err := q.myStorager.ReadObj(path, file, ext, obj)
			if err != nil {
				if l != nil {
					l.Println(err.Error())
				}
				testChan <- kvPair{"", nil}
				return
			}
			pair := kvPair{file, obj}
			testChan <- pair
		}(q.path, key, q.extension, objFactory(), q.logger)
	}
	waitAmount := len(q.toLoad)
	for waitAmount > 0 {
		temp := <-testChan
		if temp.Value != nil && temp.ID != "" {
			q.toLoad[temp.ID] = temp.Value
		}
		waitAmount--
	}
	outMap := q.toLoad
	q.toLoad = make(map[string]interface{})
	return outMap
}

func NewQAWLoader(path, extension string, getter Storager) LoadQueuer {
	tempQAW := qawLoader{path, extension, make(map[string]interface{}), getter, nil}
	return &tempQAW
}

func NewQAWLoaderLogger(path, extension string, getter Storager, l *dailyLogger.DailyLogger) LoadQueuer {
	tempQAW := qawLoader{path, extension, make(map[string]interface{}), getter, l}
	return &tempQAW
}
