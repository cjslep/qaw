package qaw

import (
	"github.com/nu7hatch/gouuid"
)

type SaveQueuer interface {
	Limit() int
	Size() int
	QueueSave(id *uuid.UUID, obj interface{}) bool
	Action() int
}

type Pusher interface {
	SaveObject(id *uuid.UUID, obj interface{}) error
}

type qawSaver struct {
	limit    int
	toSave   map[*uuid.UUID]interface{}
	myPusher Pusher
}

func (q *qawSaver) Limit() int {
	return q.limit
}

func (q *qawSaver) Size() int {
	return len(q.toSave)
}

func (q *qawSaver) QueueSave(id *uuid.UUID, obj interface{}) bool {
	if q.Limit() <= 0 || q.Size() < q.Limit() {
		q.toSave[id] = obj
		return true
	}
	return false
}

func (q *qawSaver) Action() int {
	testChan := make(chan bool)
	for key, val := range q.toSave {
		go func(i *uuid.UUID, obj interface{}, success chan<- bool) {
			err := q.myPusher.SaveObject(i, obj)
			if err != nil {
				success <- false
			} else {
				success <- true
			}
		}(key, val, testChan)
	}
	waitAmount := len(q.toSave)
	count := 0
	for waitAmount > 0 {
		temp := <-testChan
		if temp {
			count++
		}
		waitAmount--
	}
	q.toSave = make(map[*uuid.UUID]interface{})
	return count
}

func NewQAWSaver(setter Pusher) SaveQueuer {
	tempQAW := qawSaver{-1, make(map[*uuid.UUID]interface{}), setter}
	return &tempQAW
}

func NewLimitedQAWSaver(limit int, setter Pusher) SaveQueuer {
	tempQAW := qawSaver{limit, make(map[*uuid.UUID]interface{}), setter}
	return &tempQAW
}

type storagerPusherWrapper struct {
	storer Storager
}

func (w *storagerPusherWrapper) SaveObject(id *uuid.UUID, obj interface{}) error {
	return w.storer.Write("", id.String(), ".json", obj)
}

func NewFileStorerPusher(head, masterFile string) (p Pusher, e error) {
	fs, err := NewJSONFileStorer(head, masterFile)
	if err != nil {
		return nil, err
	}
	temp := storagerPusherWrapper{fs}
	return &temp, nil
}
