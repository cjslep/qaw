package qaw

import (
	"bitbucket.org/cjslep/dailyLogger"
	"github.com/coopernurse/gorp"
	"github.com/nu7hatch/gouuid"
	"database/sql"
	"fmt"
	"errors"
)

type Gorper struct {
	gorpMap *gorp.DbMap
	logger *dailyLogger.DailyLogger
	constructors map[string]func() interface{}
	connection string
	trace bool
}

func NewGorper(connection string, logger *dailyLogger.DailyLogger, trace bool) *Gorper {
	return &Gorper{nil, logger, make(map[string]func() interface{}), connection, trace}
}

func (g *Gorper) RegisterGUIDObject(i Identifiable, tableName, guidKey string, constructorFn func() interface{}) error {
	g.constructors[tableName] = constructorFn
	g.gorpMap.AddTableWithName(i, tableName).SetKeys(false, guidKey)
	return g.gorpMap.CreateTablesIfNotExists()
}

func (g *Gorper) Start() error {
	db, err := sql.Open("postgres", g.connection) //postgres://pqgotest:password@localhost/pqgotest?sslmode=verify-full
	if err != nil {
		return err
	}
	g.gorpMap = &gorp.DbMap{Db: db, Dialect: gorp.PostgresDialect{}}
	if g.trace {
		g.gorpMap.TraceOn("[GORP]", g.logger)
	}
	return nil
}

func (g *Gorper) Stop() error {
	return g.gorpMap.Db.Close()
}

func (g *Gorper) SaveObject(object Identifiable) error {
	defer func() {
        if r := recover(); r != nil {
            g.logger.Println(fmt.Sprintf("PANIC Recovery in Gorper SaveObject: %s", r))
        }
    }()
    return g.gorpMap.Insert(object)
}

func (g *Gorper) LoadObject(tableName string, objectID *uuid.UUID) (interface{}, error) {
	defer func() {
        if r := recover(); r != nil {
            g.logger.Println(fmt.Sprintf("PANIC Recovery in Gorper LoadObject: %s", r))
        }
    }()
    constructor, ok := g.constructors[tableName]
    if !ok {
    	return nil, errors.New("Gorper LoadObject: No such table with name=" + tableName)
    }
    return g.gorpMap.Get(constructor(), objectID)
}

func (g *Gorper) GetTransaction() (*gorp.Transaction, error) {
	return g.gorpMap.Begin()
}