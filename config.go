package qaw

import (
	"io/ioutil"
	"gopkg.in/v1/yaml"
)

// Data a config file must contain to initialize a FlatFileStorer
type DataConfig struct {
	DatabasePath string
	MasterFile string
}

// Loads the specified YAML file into the DataConfig struct
func LoadDataConfig(filename string) (*DataConfig, error) {
	cont, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	c := DataConfig{}
	err = yaml.Unmarshal(cont, &c)
	if err != nil {
		return nil, err
	}
	return &c, nil
}
