package qaw

import (
	"strconv"
	"testing"
)

type testDataSerialize struct {
	Id   int
	Name string
	Done int
}

func TestCreateJSONFileStorer(t *testing.T) {
	_, err := NewJSONFileStorer("test", "master.json")
	if err != nil {
		t.Errorf("Error: %q", err.Error())
	}
}

func TestWriteFile(t *testing.T) {
	ser, err := NewJSONFileStorer("test", "master.json")
	if err != nil {
		t.Errorf("Error: %q", err.Error())
	}
	ser.Start()
	temp := testDataSerialize{1, "Hello World!", 783119}
	err = ser.Write("", "temp", ".json", &temp)
	if err != nil {
		t.Errorf("Error: %q", err.Error())
	}
	ser.Stop()
}

func TestWriteFileTwice(t *testing.T) {
	ser, err := NewJSONFileStorer("test", "master.json")
	if err != nil {
		t.Errorf("Error: %q", err.Error())
	}
	ser.Start()
	temp := testDataSerialize{1, "Hello World!", 783119}
	err = ser.Write("", "temp2", ".json", &temp)
	if err != nil {
		t.Errorf("Error: %q", err.Error())
	}
	temp.Done = -1
	temp.Name = "Goodbye! :("
	err = ser.Write("", "temp2", ".json", &temp)
	if err != nil {
		t.Errorf("Error: %q", err.Error())
	}
	ser.Stop()
}

func TestWriteTwoFiles(t *testing.T) {
	ser, err := NewJSONFileStorer("test", "master.json")
	if err != nil {
		t.Errorf("Error: %q", err.Error())
	}
	ser.Start()
	temp := testDataSerialize{24, "This is a test!", 9909}
	err = ser.Write("", "another", ".json", &temp)
	if err != nil {
		t.Errorf("Error: %q", err.Error())
	}
	temp = testDataSerialize{-111, "This is a LONGER SENTENCE FOR THE test!", -10184719}
	err = ser.Write("", "third", ".json", &temp)
	if err != nil {
		t.Errorf("Error: %q", err.Error())
	}
	ser.Stop()
}

func TestWriteSameFileConcurrently(t *testing.T) {
	ser, err := NewJSONFileStorer("test", "master.json")
	ser.Start()
	if err != nil {
		t.Errorf("Error: %q", err.Error())
	}
	n := 20
	myChannels := make([]chan bool, n)
	for i := 0; i < n; i++ {
		myChannels[i] = make(chan bool, 0)
		go func(n int, aChan chan<- bool) {
			temp := testDataSerialize{n, "Hello from thread " + strconv.Itoa(n) + "!", -1 * n}
			err := ser.Write("", "concurrent", ".json", &temp)
			if err != nil {
				t.Errorf("Error: %q", err.Error())
			}
			aChan <- true
		}(i, myChannels[i])
	}
	for i := 0; i < n; i++ {
		<-myChannels[i]
	}
	ser.Stop()
}

func TestReadWriteSameFileConcurrently(t *testing.T) {
	ser, err := NewJSONFileStorer("test", "master.json")
	ser.Start()
	if err != nil {
		t.Errorf("Error: %q", err.Error())
	}
	n := 20
	myChannels := make([]chan bool, n)
	for i := 0; i < n; i++ {
		myChannels[i] = make(chan bool, 0)
		go func(n int, aChan chan<- bool) {
			temp := testDataSerialize{n, "Hello from thread " + strconv.Itoa(n) + "!", -1 * n}
			err := ser.Write("", "concurrent", ".json", &temp)
			if err != nil {
				t.Errorf("Error: %q", err.Error())
			}
			aChan <- true
		}(i, myChannels[i])

		go func() {
			temp2 := testDataSerialize{}
			err := ser.ReadObj("", "concurrent", ".json", &temp2)
			if err != nil {
				t.Errorf("Error: %q", err.Error())
			}
		}()
	}
	for i := 0; i < n; i++ {
		<-myChannels[i]
	}
	ser.Stop()
}

func TestReadFile(t *testing.T) {
	ser, err := NewJSONFileStorer("test", "master.json")
	if err != nil {
		t.Errorf("Error: %q", err.Error())
	}
	ser.Start()
	temp := testDataSerialize{1, "Junk Mail!", 1}
	err = ser.Write("", "tempRead", ".json", &temp)
	if err != nil {
		t.Errorf("Error: %q", err.Error())
	}
	res, err := ser.Read("", "tempRead", ".json")
	if err != nil {
		t.Errorf("Error: %q", err.Error())
	}
	t.Log("Result: %q", res)
	ser.Stop()
}

func TestReadFileResultObj(t *testing.T) {
	ser, err := NewJSONFileStorer("test", "master.json")
	if err != nil {
		t.Errorf("Error: %q", err.Error())
	}
	ser.Start()
	temp := testDataSerialize{1, "Junk Mail!", 1}
	err = ser.Write("", "tempRead", ".json", &temp)
	if err != nil {
		t.Errorf("Error: %q", err.Error())
	}
	temp2 := testDataSerialize{}
	err = ser.ReadObj("", "tempRead", ".json", &temp2)
	if err != nil {
		t.Errorf("Error: %q", err.Error())
	}
	t.Log("Result: %q", temp2)
	ser.Stop()
}

func BenchmarkConcurrentFileWriting(b *testing.B) {
	ser, _ := NewJSONFileStorer("test", "master.json")
	ser.Start()
	myChan := make(chan bool, b.N)
	for i := 0; i < b.N; i++ {
		go func(n int, aChan chan<- bool) {
			temp := testDataSerialize{n, "Hello from thread " + strconv.Itoa(n) + "!", -1 * n}
			_ = ser.Write("", "concurrent"+strconv.Itoa(n), ".json", &temp)
			aChan <- true
		}(i, myChan)
	}
	for i := 0; i < b.N; i++ {
		<-myChan
	}
	ser.Stop()
}

func BenchmarkConcurrentSameFileWriting(b *testing.B) {
	ser, _ := NewJSONFileStorer("test", "master.json")
	ser.Start()
	myChannels := make([]chan bool, b.N)
	for i := 0; i < b.N; i++ {
		myChannels[i] = make(chan bool)
		go func(n int, aChan chan<- bool) {
			temp := testDataSerialize{n, "Hello from thread " + strconv.Itoa(n) + "!", -1 * n}
			_ = ser.Write("", "concurrent", ".json", &temp)
			aChan <- true
		}(i, myChannels[i])
	}
	for i := 0; i < b.N; i++ {
		<-myChannels[i]
	}
	ser.Stop()
}
